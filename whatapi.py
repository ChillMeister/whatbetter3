#!/usr/bin/env python
import re
import os
import json
import time
import requests
import html.parser
from collections import OrderedDict
from bs4 import BeautifulSoup
from requests_toolbelt.multipart.encoder import MultipartEncoder

headers = {
    'Connection': 'keep-alive',
    'Cache-Control': 'max-age=0',
    'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_3)'\
        'AppleWebKit/535.11 (KHTML, like Gecko) Chrome/17.0.963.79'\
        'Safari/535.11',
    'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9'\
        ',*/*;q=0.8',
    'Accept-Encoding': 'gzip,deflate,sdch',
    'Accept-Language': 'en-US,en;q=0.8',
    'Accept-Charset': 'ISO-8859-1,utf-8;q=0.7,*;q=0.3'}

# gazelle is picky about case in searches with &media=x
media_search_map = {
    'cd': 'CD',
    'dvd': 'DVD',
    'vinyl': 'Vinyl',
    'soundboard': 'Soundboard',
    'sacd': 'SACD',
    'dat': 'DAT',
    'web': 'WEB',
    'blu-ray': 'Blu-ray'
    }

lossless_media = set(media_search_map.keys())

formats = {
    'FLAC': {
        'format': 'FLAC',
        'encoding': 'Lossless'
    },
    'V0': {
        'format' : 'MP3',
        'encoding' : 'V0 (VBR)'
    },
    '320': {
        'format' : 'MP3',
        'encoding' : '320'
    },
    'V2': {
        'format' : 'MP3', 
        'encoding' : 'V2 (VBR)'
    },
}

def allowed_transcodes(torrent):
    """Some torrent types have transcoding restrictions."""
    preemphasis = re.search(r"""pre[- ]?emphasi(s(ed)?|zed)""", torrent['remasterTitle'], flags=re.IGNORECASE)
    if preemphasis:
        return []
    else:
        return formats.keys()

class LoginException(Exception):
    pass

class RequestException(Exception):
    pass

class WhatAPI:
    def __init__(self, username=None, password=None):
        self.session = requests.Session()
        self.session.headers.update(headers)
        self.username = username
        self.password = password
        self.authkey = None
        self.passkey = None
        self.userid = None
        self.tracker = "http://tracker.what.cd:34000/"
        self.last_request = time.time()
        self.rate_limit = 2.0 # seconds between requests
        self._login()

    def _login(self):
        '''Logs in user and gets authkey from server'''
        loginpage = 'https://what.cd/login.php'
        data = {'username': self.username,
                'password': self.password}
        r = self.session.post(loginpage, data=data)
        if r.status_code != 200:
            raise LoginException
        accountinfo = self.request('index')
        self.authkey = accountinfo['authkey']
        self.passkey = accountinfo['passkey']
        self.userid = accountinfo['id']

    def logout(self):
        self.session.get("https://what.cd/logout.php?auth=%s" % self.authkey)

    def request(self, action, **kwargs):
        '''Makes an AJAX request at a given action page'''
        while time.time() - self.last_request < self.rate_limit:
            time.sleep(0.1)

        ajaxpage = 'https://what.cd/ajax.php'
        params = {'action': action}
        if self.authkey:
            params['auth'] = self.authkey
        params.update(kwargs)
        r = self.session.get(ajaxpage, params=params, allow_redirects=False)
        self.last_request = time.time()
        try:
            parsed = r.json()
            if parsed['status'] != 'success':
                raise RequestException
            return parsed['response']
        except ValueError:
            raise RequestException

    def request_html(self, action, **kwargs):
        while time.time() - self.last_request < self.rate_limit:
            time.sleep(0.1)

        ajaxpage = 'https://what.cd/' + action
        if self.authkey:
            kwargs['auth'] = self.authkey
        r = self.session.get(ajaxpage, params=kwargs, allow_redirects=False)
        self.last_request = time.time()
        return r.content
    
    def get_artist(self, id=None, format='MP3', best_seeded=True):
        res = self.request('artist', id=id)
        torrentgroups = res['torrentgroup']
        keep_releases = []
        for release in torrentgroups:
            torrents = release['torrent']
            best_torrent = torrents[0]
            keeptorrents = []
            for t in torrents:
                if t['format'] == format:
                    if best_seeded:
                        if t['seeders'] > best_torrent['seeders']:
                            keeptorrents = [t]
                            best_torrent = t
                    else:
                        keeptorrents.append(t)
            release['torrent'] = list(keeptorrents)
            if len(release['torrent']):
                keep_releases.append(release)
        res['torrentgroup'] = keep_releases
        return res

    def snatched(self, skip=None, media=lossless_media):
        if not media.issubset(lossless_media):
            raise ValueError('Unsupported media type %s' % (media - lossless_media).pop())

        # gazelle doesn't currently support multiple values per query
        # parameter, so we have to search a media type at a time;
        # unless it's all types, in which case we simply don't specify
        # a 'media' parameter (defaults to all types).

        if media == lossless_media:
            media_params = ['']
        else:
            media_params = ['&media=%s' % media_search_map[m] for m in media]

        url = 'https://what.cd/torrents.php?type=snatched&userid=%s&format=FLAC' % self.userid
        for mp in media_params:
            page = 1
            done = False
            pattern = re.compile('torrents.php\?id=(\d+)&amp;torrentid=(\d+)')
            while not done:
                content = self.session.get(url + mp + "&page=%s" % page).text
                for groupid, torrentid in pattern.findall(content):
                    if skip is None or torrentid not in skip:
                        yield int(groupid), int(torrentid)
                done = 'Next &gt;' not in content
                page += 1

    def upload(self, group, torrent, new_torrent, torrent_format, description=[]):
        url = 'https://what.cd/upload.php?groupid=%s' % group['group']['id']
        response = self.session.get(url)

        soup = BeautifulSoup(response.text, 'html.parser')
        form_auth = soup.find('input', {'name':'auth'})['value']
        form_groupid = soup.find('input', {'name':'groupid'})['value']
        form_type = soup.find('input', {'name':'type'})['value']

        release_desc = '\n'.join(description)

        with open(new_torrent, 'rb') as f:
            form_fields = OrderedDict([
                ('submit', 'true'),
                ('auth', form_auth),
                ('groupid', form_groupid),
                ('type', form_type),
                ('file_input', (os.path.basename(new_torrent), f, 'application/x-bittorrent')),
                ('remaster_year', torrent['remasterYear'] if torrent['remastered'] else ''),
                ('remaster_title', torrent['remasterTitle'] if torrent['remastered'] else ''),
                ('remaster_record_label', torrent['remasterRecordLabel'] if torrent['remastered'] else ''),
                ('remaster_catalogue_number', torrent['remasterCatalogueNumber'] if torrent['remastered'] else ''),
                ('format', formats[torrent_format]['format']),
                ('bitrate', formats[torrent_format]['encoding']),
                ('other_bitrate', ''),
                ('logfiles[]', ('', '', 'application/octet-stream')),
                ('media', torrent['media']),
                ('release_desc', release_desc if release_desc else '')
            ])
            if torrent['remastered']:
                form_fields['remaster'] = 'on'

            form_multipart = MultipartEncoder(fields=form_fields)
            fixed_form_multipart = form_multipart.to_string().replace(b'\"logfiles[]\"',b'\"logfiles[]\"; filename=\"\"')

            my_headers = dict(headers)
            my_headers.update({'Content-Type': form_multipart.content_type})
            return self.session.post(url, data=fixed_form_multipart, headers=my_headers)

    def set_24bit(self, torrent):
        url = 'https://what.cd/torrents.php?action=edit&id={}'.format(torrent['id'])
        response = self.session.get(url)

        soup = BeautifulSoup(response.text, 'html.parser')
        form_auth = soup.find('input', {'name':'auth'})['value']
        form_torrentid = soup.find('input', {'name':'torrentid'})['value']
        form_type = soup.find('input', {'name':'type'})['value']

        form_fields = OrderedDict([
            ('submit', 'true'),
            ('auth', form_auth),
            ('action', 'takeedit'),
            ('torrentid', form_torrentid),
            ('type', form_type),
            ('remaster_year', torrent['remasterYear'] if torrent['remastered'] else ''),
            ('remaster_title', torrent['remasterTitle'] if torrent['remastered'] else ''),
            ('remaster_record_label', torrent['remasterRecordLabel'] if torrent['remastered'] else ''),
            ('remaster_catalogue_number', torrent['remasterCatalogueNumber'] if torrent['remastered'] else ''),
            ('format', 'FLAC'),
            ('bitrate', '24bit Lossless'),
            ('other_bitrate', ''),
            ('media', torrent['media']),
            ('release_desc', torrent['description'])
        ])
        if torrent['remastered']:
            form_fields['remaster'] = 'on'
            groupremasters_field = soup.find('select', {'name':'groupremasters'}).find('option', {'selected':'selected'})
            form_fields['groupremasters'] = groupremasters_field['value'] if groupremasters_field else ''

        form_multipart = MultipartEncoder(fields=form_fields)

        my_headers = dict(headers)
        my_headers.update({'Content-Type': form_multipart.content_type})
        res = self.session.post(url, data=form_multipart, headers=my_headers)

        # Sanity check
        new_torrent_data = self.get_torrent_info(torrent['id'])
        for key in new_torrent_data.keys():
            if key not in ['encoding', 'infoHash', 'snatched', 'leechers', 'seeders'] and new_torrent_data[key] != torrent[key]:
                print(new_torrent_data)
                print(torrent)
                print(new_torrent_data[key])
                print(torrent[key])
                print(key)
                raise Exception('Data mismatch!')

        return res

    def release_url(self, group, torrent):
        return "https://what.cd/torrents.php?id=%s&torrentid=%s#torrent%s" % (group['group']['id'], torrent['id'], torrent['id'])

    def permalink(self, torrent):
        return "https://what.cd/torrents.php?torrentid=%s" % torrent['id']

    def get_better(self, type=3):
        p = re.compile(r'(torrents\.php\?action=download&(?:amp;)?id=(\d+)[^"]*).*(torrents\.php\?id=\d+(?:&amp;|&)torrentid=\2\#torrent\d+)', re.DOTALL)
        out = []
        data = self.request_html('better.php', method='transcode', type=type)
        for torrent, id, perma in p.findall(data):
            out.append({
                'permalink': perma.replace('&amp;', '&'),
                'id': int(id),
                'torrent': torrent.replace('&amp;', '&'),
            })
        return out

    def get_torrent(self, torrent_id):
        '''Downloads the torrent at torrent_id using the authkey and passkey'''
        while time.time() - self.last_request < self.rate_limit:
            time.sleep(0.1)

        torrentpage = 'https://ssl.what.cd/torrents.php'
        params = {'action': 'download', 'id': torrent_id}
        if self.authkey:
            params['authkey'] = self.authkey
            params['torrent_pass'] = self.passkey
        r = self.session.get(torrentpage, params=params, allow_redirects=False)

        self.last_request = time.time() + 2.0
        if r.status_code == 200 and 'application/x-bittorrent' in r.headers['content-type']:
            return r.content
        return None

    def get_torrent_info(self, torrentid):
        return self.request('torrent', id=torrentid)['torrent']

def unescape(text):
    return html.parser.HTMLParser().unescape(text)
